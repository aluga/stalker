package ml.agapps.stalker.stalker;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;

public class StreamInput extends AsyncTask {
    private Socket mSocket;
    private StreamPreview streamPreview;
    private static final String TAG = "inputstream";
    private String mIP = "192.168.1.10";
    private int mPort = 3006;
    private OnStreamListener streamListener;

    public StreamInput(StreamPreview preview, String ip, int port, OnStreamListener listener){
        streamPreview = preview;
        mIP = ip;
        mPort = port;
        streamListener=listener;
        execute();
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        try {
            Log.i(TAG, "start");
            mSocket = new Socket();
            mSocket.connect(new InetSocketAddress(mIP, mPort), 10000);
            BufferedOutputStream outputStream = new BufferedOutputStream(mSocket.getOutputStream());
            BufferedInputStream inputStream = new BufferedInputStream(mSocket.getInputStream());

            Log.i(TAG, "connected");
            byte[] buff = new byte[1024];

            String trackerId = "0";
            outputStream.write(trackerId.getBytes());
            outputStream.flush();
            ByteArrayOutputStream dataBuff = new ByteArrayOutputStream(streamPreview.getLength());
            ByteArrayOutputStream imageBuff = new ByteArrayOutputStream(streamPreview.getLength());

            int len =0;
            int total = 0;
            int frameSize=0;
            byte[] tempData=null;
            byte[] size = new byte[4];
            boolean isToBreak = false;
            while (true) {
                Log.i(TAG, "receiving a frame");

                while((len = inputStream.read(buff)) != -1 && !Thread.currentThread().isInterrupted()) {
                    int offset = 0;
                    if(frameSize==0 && total==0){
                        Log.d(TAG, "get piece");
                        size = copyNbytes(size, buff, 0, 0);
                        offset = 4;
                        len -= 4;
                    }
                    else if(frameSize==0 && total < 4){
                        Log.d(TAG, "mount piece");
                        size = copyNbytes(size, buff, total, 0);
                        total=0;
                    }

                    if(frameSize==0){
                        Log.d(TAG, "get frame");
                        frameSize = byte42Int(size, 0);
                        if(frameSize > streamPreview.getLength()) {
                            isToBreak=true;
                            break;
                        }
                    }
                    Log.d(TAG, "frame: "+frameSize+";"+len);
                    dataBuff.write(buff, offset, len);
                    Log.d(TAG, "write data");
                    total += len;
                    if (total >= frameSize) {
                        Log.d(TAG, "break");
                        break;
                    }
                }
                if(Thread.currentThread().isInterrupted() || isToBreak) break;

                tempData = dataBuff.toByteArray();
                Log.d(TAG, "got the data frame temp");
                imageBuff.write(tempData, 0, frameSize);
                Log.d(TAG, "got the data frame buff");
                try {
                    byte[] data = imageBuff.toByteArray();
                    Log.d(TAG,"get the byte array");
                    streamPreview.draw(data);
                    Log.d(TAG, "draw");
                } catch (Exception e){
                    Log.e(TAG, e.toString());
                }

                imageBuff.reset();
                dataBuff.reset();
                Log.d(TAG, "total: "+total);
                total -= frameSize;
                size = copyNbytes(size,tempData,0,frameSize);
                if(total>4) {
                    total -= 4;
                    dataBuff.write(tempData, frameSize+4, total);
                }
                frameSize=0;

            }

            outputStream.close();
            inputStream.close();
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            try {
                mSocket.close();
                mSocket = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private byte[] copyNbytes(byte[] dst, byte[] src, int offdst, int offsrc){
        for(int i=offdst; i<dst.length && (offsrc+i) < src.length; i++) dst[i] = src[offsrc+i];
        return dst;
    }

    private int byte42Int(byte[] data, int offset){
        ByteBuffer b = ByteBuffer.allocate(4);
        for(int i=0; i<4; i++) b.put(i,data[i+offset]);
        return b.getInt();
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        streamListener.onStop();
    }
}
