package ml.agapps.stalker.stalker;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;

public class StreamPreview extends SurfaceView {
    private int width=480;
    private int height=640;

    public StreamPreview(Context context) {
        super(context);
        SurfaceHolder holder = this.getHolder();
        holder.setFixedSize(width,height);
    }

    public int getLength(){
        return width*height*3;
    }

    public void draw(byte[] data){
        Canvas canvas = null;
        Log.d("draw", "start bytes: "+data[0]+";"+data[1]+";"+data[data.length-2]+";"+data[data.length-1]+";");
        Log.d("draw",""+data.length);
        Bitmap imageJPG = BitmapFactory.decodeByteArray(data, 0, data.length);
        Matrix matrix = new Matrix();
        matrix.postRotate(90);
        Bitmap image = Bitmap.createBitmap(imageJPG, 0,0, imageJPG.getWidth(), imageJPG.getHeight(), matrix, true);
        Log.d("draw", "image: "+image+"; width:"+image.getWidth()+"; height:"+image.getHeight());

        SurfaceHolder holder = getHolder();
        canvas = holder.lockCanvas(null);

        try {
            synchronized (holder) {
                if(canvas != null && image != null)canvas.drawBitmap(image, 0,0, new Paint());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (canvas != null) {
                holder.unlockCanvasAndPost(canvas);
            }
        }
    }
}
