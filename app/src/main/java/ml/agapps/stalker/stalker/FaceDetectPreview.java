package ml.agapps.stalker.stalker;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.RectF;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;

public class FaceDetectPreview extends SurfaceView {
    private static final String TAG = "FaceDetect";
    private FaceDetector faceDetector;
    private static final int width=480;
    private static final int height=640;
    private byte[] imageData;
    private int[] faceLocation = new int[4];
    private Socket mSocket;
    private String mIP = "192.168.1.10";
    private int mPort = 3003;

    public FaceDetectPreview(Context context, String ip) {
        super(context);
        setBackgroundColor(Color.TRANSPARENT);
        getHolder().setFormat(PixelFormat.TRANSPARENT);
        getHolder().setFixedSize(width,height);
        faceDetector = new FaceDetector
                .Builder(context).setTrackingEnabled(false)
                .build();
        if(ip!=null)
            mIP = ip;
    }

    public void clean(){
        imageData=null;
        SurfaceHolder holder = getHolder();
        Canvas canvas = holder.lockCanvas(null);
        try {
            synchronized (holder) {
                if(canvas != null) {
                    canvas.drawColor(Color.TRANSPARENT);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (canvas != null) {
                holder.unlockCanvasAndPost(canvas);
            }
        }
    }


    public void draw(byte[] data){
        Log.i(TAG, "getting images");
        Bitmap imageJPG = BitmapFactory.decodeByteArray(data, 0, data.length);
        Matrix matrix = new Matrix();
        matrix.postRotate(90);
        Bitmap image = Bitmap.createBitmap(imageJPG, 0,0, imageJPG.getWidth(), imageJPG.getHeight(), matrix, true);

        Frame frame = new Frame.Builder().setBitmap(image).build();
        SparseArray<Face> faces = faceDetector.detect(frame);

        SurfaceHolder holder = getHolder();
        Canvas canvas = holder.lockCanvas(null);

        try {
            synchronized (holder) {
                if(canvas != null && image != null) {
                    canvas.drawBitmap(image, 0, 0, new Paint());
                    if(faces.size()>0) {
                        imageData = data;
                        Face thisFace = faces.valueAt(0);
                        float x = thisFace.getPosition().x;
                        float y = thisFace.getPosition().y;
                        float w = thisFace.getWidth();
                        float h = thisFace.getHeight();
                        RectF faceRect = new RectF(x, y, x+w, y+h);

                        // rotate the face rect
                        if(y<0)
                            faceLocation[0] = 0;
                        else
                            faceLocation[0] = Math.round(y);

                        if(x<0)
                            faceLocation[1] = Math.round(0);
                        else
                            faceLocation[1] = Math.round(x);

                        faceLocation[2] = Math.round(h);
                        faceLocation[3] = Math.round(w);

                        Log.d(TAG, "x:"+x+"; w:"+w+"; y:"+y+"; h:"+h);

                        Paint style = new Paint();
                        style.setColor(Color.YELLOW);
                        style.setStyle(Paint.Style.STROKE);
                        style.setStrokeWidth(5);
                        canvas.drawRoundRect(faceRect, 2, 2, style);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (canvas != null) {
                holder.unlockCanvasAndPost(canvas);
            }
        }
    }

    public void sendFace() {
        if(imageData == null) return;
        try {
            mSocket = new Socket();
            mSocket.connect(new InetSocketAddress(mIP, mPort), 10000);
            BufferedOutputStream outputStream = new BufferedOutputStream(mSocket.getOutputStream());
            BufferedInputStream inputStream = new BufferedInputStream(mSocket.getInputStream());

            JsonObject jsonObj = new JsonObject();
            jsonObj.addProperty("type", "detect");
            jsonObj.addProperty("length", width*height);
            jsonObj.addProperty("width", width);
            jsonObj.addProperty("height", height);

            byte[] buff = new byte[256];
            int len = 0;
            String msg = null;
            outputStream.write(jsonObj.toString().getBytes());
            outputStream.flush();

            while ((len = inputStream.read(buff)) != -1) {
                msg = new String(buff, 0, len);

                // JSON analysis
                JsonParser parser = new JsonParser();
                boolean isJSON = true;
                JsonElement element = null;
                try {
                    element = parser.parse(msg);
                } catch (JsonParseException e) {
                    Log.e(TAG, "exception: " + e);
                    isJSON = false;
                }
                if (isJSON && element != null) {
                    JsonObject obj = element.getAsJsonObject();
                    element = obj.get("state");
                    if (element != null && element.getAsString().equals("ok")) {

                        // prepare data
                        ByteBuffer size = ByteBuffer.allocate(4);
                        size.putInt(imageData.length+4*4);
                        ByteBuffer x1 = ByteBuffer.allocate(4);
                        x1.putInt(faceLocation[0]);
                        ByteBuffer y1 = ByteBuffer.allocate(4);
                        y1.putInt(faceLocation[1]);
                        ByteBuffer faceWidth = ByteBuffer.allocate(4);
                        faceWidth.putInt(faceLocation[2]);
                        ByteBuffer faceHeight = ByteBuffer.allocate(4);
                        faceHeight.putInt(faceLocation[3]);

                        // send data
                        outputStream.write(size.array());
                        outputStream.write(x1.array());
                        outputStream.write(y1.array());
                        outputStream.write(faceWidth.array());
                        outputStream.write(faceHeight.array());
                        outputStream.write(imageData);
                        outputStream.flush();
                        Log.d(TAG, "size sended: " + imageData.length);
                        break;
                    }
                } else {
                    break;
                }
            }

            outputStream.close();
            inputStream.close();
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            try {
                mSocket.close();
                mSocket = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
