package ml.agapps.stalker.stalker;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;

public class DetectActivity extends AppCompatActivity {
    private static final String TAG = "DetectActivity";

    private FrameLayout frameLayout;
    private FloatingActionButton sendButton;
    private FloatingActionButton cancelButton;

    private CameraManager cameraManager;
    private CameraPreview cameraPreview;
    private FaceDetectPreview faceDetectPreview;

    private boolean isCameraActivated=false;
    private boolean isSending=false;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detect);

        String host = getIntent().getExtras().getString("host");

        frameLayout = (FrameLayout) findViewById(R.id.detect_preview);
        sendButton = (FloatingActionButton) findViewById(R.id.detect_send);
        cancelButton = (FloatingActionButton) findViewById(R.id.detect_cancel);

        cameraManager = new CameraManager(this);
        cameraPreview = new CameraPreview(this);
        faceDetectPreview = new FaceDetectPreview(this, host);
        frameLayout.addView(faceDetectPreview,0);

        frameLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(isCameraActivated) {
                    stopPreview();
                    faceDetectPreview.draw(cameraPreview.takePicture());
                    showButtons();
                }
                return false;
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isSending) {
                    faceDetectPreview.clean();
                    startPreview();
                }
            }
        });

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isSending=true;
                SendDetectedFace s = new SendDetectedFace(new OnStreamListener() {
                    @Override
                    public void onStop() {
                        isSending=false;
                        faceDetectPreview.clean();
                        startPreview();
                    }
                });
                s.execute();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(isCameraActivated)
            stopPreview();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!isCameraActivated)
            startPreview();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    private void hideButtons(){
        sendButton.setVisibility(View.GONE);
        cancelButton.setVisibility(View.GONE);
    }

    private void showButtons(){
        sendButton.setVisibility(View.VISIBLE);
        cancelButton.setVisibility(View.VISIBLE);
    }

    private void startPreview(){
        hideButtons();
        cameraManager.onResume();
        cameraPreview.setCamera(cameraManager.getCamera());
        isCameraActivated=true;
        frameLayout.addView(cameraPreview);
    }

    private void stopPreview(){
        frameLayout.removeView(cameraPreview);
        cameraPreview.onPause();
        cameraManager.onPause();
        isCameraActivated=false;
    }

    private class SendDetectedFace extends AsyncTask{
        private OnStreamListener onStreamListener;

        public SendDetectedFace(OnStreamListener listener){
            onStreamListener = listener;
        }

        protected Object doInBackground(Object[] objects) {
            faceDetectPreview.sendFace();
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            onStreamListener.onStop();
        }
    }

}
