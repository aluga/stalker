package ml.agapps.stalker.stalker;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.Toast;

public class TrackerActivity extends AppCompatActivity {

    private StreamPreview streamPreview;
    private StreamInput streamInput;
    private String host;
    private boolean isStreaming=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracker);

        Intent intent = getIntent();
        host = intent.getExtras().getString("host");

        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.tracker_preview);
        streamPreview = new StreamPreview(this);
        frameLayout.addView(streamPreview);
    }

    @Override
    protected void onPause() {
        super.onPause();
        closeStreamInput();
        isStreaming=false;
    }

    private void startInputStream(){
        streamInput = new StreamInput(streamPreview, host, 3006, new OnStreamListener() {
            @Override
            public void onStop() {
                if (isStreaming) {
                    new java.util.Timer().schedule(
                            new java.util.TimerTask() {
                                @Override
                                public void run() {
                                    startInputStream();
                                }
                            },
                            500
                    );

                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        isStreaming=true;
        Toast.makeText(TrackerActivity.this, "connecting...", Toast.LENGTH_LONG).show();
        startInputStream();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        closeStreamInput();
    }

    private void closeStreamInput(){
        if(streamInput == null)
            return;
        streamInput.cancel(true);
        streamInput = null;
    }
}
