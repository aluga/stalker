package ml.agapps.stalker.stalker;

public class ServerStatus {
    String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
