package ml.agapps.stalker.stalker;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Switch;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MainActivity extends AppCompatActivity {

    private LinearLayout menuHome;
    private ConstraintLayout menuStreamings;
    private FloatingActionButton addTrackerButton;
    private FloatingActionButton refreshButton;
    private Switch switchStreaming;
    private TextInputEditText hostInput;
    private FrameLayout frameLayout;

    private CameraManager cameraManager;
    private CameraPreview cameraPreview;
    private StreamService streamService;
    private StreamOutput streamOutput;
    private String host;
    private int port;
    private boolean isStreamming=false;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    menuStreamings.setVisibility(View.GONE);
                    menuHome.setVisibility(View.VISIBLE);
                    return true;
                case R.id.navigation_dashboard:
                    menuHome.setVisibility(View.GONE);
                    menuStreamings.setVisibility(View.VISIBLE);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        menuHome = (LinearLayout) findViewById(R.id.menu_home);
        menuStreamings = (ConstraintLayout) findViewById(R.id.menu_streamings);
        switchStreaming = (Switch) findViewById(R.id.switch_streaming);
        hostInput = (TextInputEditText) findViewById(R.id.host_input);
        frameLayout = (FrameLayout) findViewById(R.id.camera_preview);

        menuStreamings.setVisibility(View.GONE);
        addTrackerButton = (FloatingActionButton) findViewById(R.id.add_tracker_button);
        addTrackerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isStreamming){
                    stopStream();
                }
                Intent intent = new Intent(MainActivity.this, DetectActivity.class);
                host = hostInput.getText().toString();
                intent.putExtra("host", host);
                startActivity(intent);
            }
        });

        refreshButton = (FloatingActionButton) findViewById(R.id.refresh_button);
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, TrackerActivity.class);
                host = hostInput.getText().toString();
                intent.putExtra("host", host);
                startActivity(intent);
            }
        });

        host = getResources().getString(R.string.host);
        hostInput.setText(host);

        switchStreaming.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isStreamming=isChecked;
                if(isChecked){
                    startStream();
                }
                else {
                    stopStream();
                }
            }
        });

        cameraManager = new CameraManager(this);
        cameraPreview = new CameraPreview(this);
        loadHTTPStreamService();
    }

    private boolean askCameraPermission(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 101);
            return false;
        }
        return true;
    }

    private void loadHTTPStreamService(){
        String port = getResources().getString(R.string.http_port);
        String url = "http://"+host+":"+port;
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(url).build();
        streamService = retrofit.create(StreamService.class);
    }

    public void startStreamServer(){
        streamOutput = new StreamOutput(cameraPreview, host, port, new OnStreamListener() {
            @Override
            public void onStop() {
                if (isStreamming) {
                    new java.util.Timer().schedule(
                            new java.util.TimerTask() {
                                @Override
                                public void run() {
                                    startStreamServer();
                                }
                            },
                            500
                    );

                }
            }
        });
    }

    public void startStream(){
        if(askCameraPermission()) {
            cameraManager.onResume();
            cameraPreview.setCamera(cameraManager.getCamera());
            cameraPreview.onResume();
            frameLayout.addView(cameraPreview);

            host = hostInput.getText().toString();
            hostInput.setEnabled(false);
            port = Integer.parseInt(getResources().getString(R.string.tcp_port));
            startStreamServer();
            loadHTTPStreamService();
        }
        else{
            switchStreaming.setChecked(false);
            isStreamming=false;
        }
    }


    public void stopStream(){
        frameLayout.removeAllViews();
        hostInput.setEnabled(true);
        closeStreamOutput();
        cameraPreview.onPause();
        cameraManager.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(isStreamming && streamOutput == null) {
            startStream();
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if(isStreamming) {
            closeStreamOutput();
        }
    }

//    private void closeTrans(){
//        if(trans == null)
//            return;
//        trans.cancel(true);
//        trans = null;
//    }

    private void closeStreamOutput(){
        if(streamOutput == null)
            return;
        streamOutput.cancel(true);
        streamOutput = null;
    }
}
