package ml.agapps.stalker.stalker;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;

public class StreamOutput extends AsyncTask<String,String,String> {
    private Socket mSocket;
    private CameraPreview mCameraPreview;
    private static final String TAG = "outputstream";
    private String mIP = "192.168.1.10";
    private int mPort = 3003;
    private OnStreamListener streamListener;

    public StreamOutput(CameraPreview preview, String ip, int port, OnStreamListener listener) {
        mCameraPreview = preview;
        mIP = ip;
        mPort = port;
        streamListener = listener;
        execute();
    }

    @Override
    protected String doInBackground(String... strings) {

        try {
            mSocket = new Socket();
            mSocket.connect(new InetSocketAddress(mIP, mPort), 10000);
            BufferedOutputStream outputStream = new BufferedOutputStream(mSocket.getOutputStream());
            BufferedInputStream inputStream = new BufferedInputStream(mSocket.getInputStream());

            JsonObject jsonObj = new JsonObject();
            jsonObj.addProperty("type", "data");
            jsonObj.addProperty("length", mCameraPreview.getPreviewLength());
            jsonObj.addProperty("width", mCameraPreview.getPreviewWidth());
            jsonObj.addProperty("height", mCameraPreview.getPreviewHeight());

            byte[] buff = new byte[256];
            int len = 0;
            String msg = null;
            outputStream.write(jsonObj.toString().getBytes());
            outputStream.flush();

            while ((len = inputStream.read(buff)) != -1) {
                msg = new String(buff, 0, len);

                // JSON analysis
                JsonParser parser = new JsonParser();
                boolean isJSON = true;
                JsonElement element = null;
                try {
                    element = parser.parse(msg);
                } catch (JsonParseException e) {
                    Log.e(TAG, "exception: " + e);
                    isJSON = false;
                }
                if (isJSON && element != null) {
                    JsonObject obj = element.getAsJsonObject();
                    element = obj.get("state");
                    if (element != null && element.getAsString().equals("ok")) {
                        // send data
                        byte[] oldData = null;
                        while (true) {
                            byte[] data = mCameraPreview.getImageBuffer();
                            if (data == null || data == oldData) continue;
                            ByteBuffer b = ByteBuffer.allocate(4);
                            b.putInt(data.length);
                            byte[] size = b.array();
                            outputStream.write(size);
                            outputStream.write(data);
                            outputStream.flush();
                            Log.d(TAG, "size sended: "+data.length);
                            oldData=data;
                            if (Thread.currentThread().isInterrupted())
                                break;
                        }

                        break;
                    }
                } else {
                    break;
                }
            }

            outputStream.close();
            inputStream.close();
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            try {
                mSocket.close();
                mSocket = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        streamListener.onStop();
    }
}

