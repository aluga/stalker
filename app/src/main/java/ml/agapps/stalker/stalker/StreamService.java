package ml.agapps.stalker.stalker;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface StreamService {

    @POST("track")
    Call<ServerStatus> createTrack(@Body byte[] image);

    @POST("stream")
    Call<ServerStatus> sendImage(@Body byte[] image);
}
