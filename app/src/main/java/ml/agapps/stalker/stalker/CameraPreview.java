package ml.agapps.stalker.stalker;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ImageFormat;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;


public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {
    private SurfaceHolder mHolder;
    private Camera mCamera;
    private static final String TAG = "camera";
    private Size mPreviewSize;
    private byte[] mImageData;
    private LinkedList<byte[]> mQueue = new LinkedList<byte[]>();
    private static final int MAX_BUFFER = 6;
    private byte[] mLastFrame = null;
    private int mFrameLength;
    private byte[] picture;

    public CameraPreview(Context context){
        super(context);
    }

    public void setCamera(Camera camera) {
        mCamera = camera;

        if(mHolder==null) {
            mHolder = getHolder();
            mHolder.addCallback(this);
            // deprecated setting, but required on Android versions prior to 3.0
            mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        }

        Parameters params = mCamera.getParameters();
        List<Size> sizes = params.getSupportedPreviewSizes();
        for (Size s : sizes) {
            Log.i(TAG, "preview size = " + s.width + ", " + s.height);
        }

        params.setPreviewSize(640, 480); // set preview size. smaller is better
        params.setPreviewFormat(ImageFormat.NV21);
        mCamera.setParameters(params);
        mCamera.setDisplayOrientation(90); // set image on preview layout as vertical.

        mPreviewSize = mCamera.getParameters().getPreviewSize();
        Log.i(TAG, "preview size = " + mPreviewSize.width + ", " + mPreviewSize.height);

        int format = mCamera.getParameters().getPreviewFormat();
        mFrameLength = mPreviewSize.width * mPreviewSize.height;
        mCamera.setFaceDetectionListener(new Camera.FaceDetectionListener() {
            @Override
            public void onFaceDetection(Camera.Face[] faces, Camera camera) {
                Log.d("kkkkkkk", "#####################################");
            }
        });
    }

    public void surfaceCreated(SurfaceHolder holder) {
        try {
            mCamera.setPreviewDisplay(holder);
            mCamera.startPreview();
        } catch (IOException e) {
            Log.d(TAG, "Error setting camera preview: " + e.getMessage());
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {

        if (mHolder.getSurface() == null){
            return;
        }

        try {
            mCamera.stopPreview();
            resetBuff();

        } catch (Exception e){

        }

        try {
            mCamera.setPreviewCallback(mPreviewCallback);
            mCamera.setPreviewDisplay(mHolder);
            mCamera.startPreview();

        } catch (Exception e){
            Log.d(TAG, "Error starting camera preview: " + e.getMessage());
        }
    }

    public byte[] getImageBuffer() {
        synchronized (mQueue) {
            if (mQueue.size() > 0) {
                mLastFrame = mQueue.poll();
            }
        }

        return mLastFrame;
    }

    public byte[] takePicture(){
        return picture;
    }

    private void resetBuff() {

        synchronized (mQueue) {
            if(mQueue.size()>0)
                picture = mQueue.getLast();
            mQueue.clear();
            mLastFrame = null;
        }
    }

    public int getPreviewLength() {
        return mFrameLength;
    }

    public int getPreviewWidth() {
        return mPreviewSize.width;
    }

    public int getPreviewHeight() {
        return mPreviewSize.height;
    }

    public void onResume(){
        if (mCamera != null){
            resetBuff();
            mCamera.setPreviewCallback(mPreviewCallback);
            mCamera.startPreview();
        }
    }

    public void onPause() {
        if (mCamera != null) {
            mCamera.setPreviewCallback(null);
            mCamera.stopPreview();
        }
        resetBuff();
    }

    private byte[] compressImage(byte[] data){

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        YuvImage yuvImage = new YuvImage(data, ImageFormat.NV21, mPreviewSize.width, mPreviewSize.height, null);
        yuvImage.compressToJpeg(new Rect(0, 0, mPreviewSize.width, mPreviewSize.height), 50, out);
        byte[] imageBytes = out.toByteArray();
        Log.d("create", "start bytes: "+imageBytes[0]+";"+imageBytes[1]+";"+imageBytes[imageBytes.length-2]+";"+imageBytes[imageBytes.length-1]+";");
        return imageBytes;

    }

    private Camera.PreviewCallback mPreviewCallback = new PreviewCallback() {

        @Override
        public void onPreviewFrame(byte[] data, Camera camera) {
            byte[] convertData = compressImage(data);
            if(convertData==null) return;

            synchronized (mQueue) {
                if (mQueue.size() == MAX_BUFFER) {
                    mQueue.poll();
                }
                mQueue.add(convertData);
            }
        }
    };
}